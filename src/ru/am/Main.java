package ru.am;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Map<Integer, String> java = new HashMap<>();
        java.put(1, "a");
        java.put(2, "b");
        java.put(3, "c");

        {
            Map<Integer, String> my = new MyHashMap<>(java);
            System.out.println(my.isEmpty());
            my.entrySet().forEach(System.out::println);
            System.out.println(my.size());
            System.out.println(my.containsKey(2));
            System.out.println(my.containsKey(4));
            System.out.println(my.containsValue("c"));
            System.out.println(my.containsValue("d"));
            System.out.println(my.get(1));
            System.out.println(my.get(4));
            System.out.println(my.put(1, "collision"));
            System.out.println(my.containsValue("collision"));
            System.out.println(my.remove(2));
            System.out.println(my.keySet());
            System.out.println(my.values());

        }
    }
}