package ru.am;

import java.util.*;

public class MyHashMap<K, V> implements Map<K, V> {

    private static class Node<K, V> implements Map.Entry<K, V> {
        int hash;
        K key;
        V value;
        Node<K, V> next;

        Node(int hash, K key, V value, Node<K,V> next) {
            this.hash = hash;
            this.key = key;
            this.value = value;
            this.next = next;
        }


        public K getKey() {
            return key;
        }


        public V getValue() {
            return value;
        }


        public String toString() {
            return key + "=" + value;
        }


        public int hashCode() {
            return key.hashCode();
        }


        public V setValue(V newValue) {
            V old = value;
            value = newValue;
            return old;
        }


        public boolean equals(Object o) {
            if (o == this) {
                return true;
            }
            if (o instanceof Map.Entry<?, ?>) {
                return ((Entry<?, ?>) o).getKey().equals(key)
                        && ((Entry<?, ?>) o).getValue().equals(value);
            }
            return false;
        }
    }

    private Node<K, V>[] table;
    private int size;
    private int fill;
    private float fillRatio;
    private int maxSize;

    public MyHashMap() {
        size = 16;
        fillRatio = 0.75f;
        table = new Node[size];
        maxSize = (int) (size*fillRatio);
        fill = 0;
    }

    public MyHashMap(Map<K, V> m) {
        size = 16;
        fillRatio = 0.75f;
        table = new Node[size];
        maxSize = (int) (size*fillRatio);
        fill = 0;
        this.putAll(m);
    }

    public MyHashMap(int capacity) {
        size = capacity;
        fillRatio = 0.75f;
        table = new Node[size];
        maxSize = (int) (size*fillRatio);
        fill = 0;
    }

    public MyHashMap(int capacity, float fillRatio) {
        if (fillRatio > 1){
            fillRatio = 0.75f;
        }
        size = capacity;
        table = new Node[size];
        maxSize = (int) (size*fillRatio);
        fill = 0;
    }

    public int size() {
        return fill;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public boolean containsKey(Object key) {
        for (Node<K, V> n: table) {
            if (n != null) {
                Node<K, V> buffer = n;
                while (true) {
                    if (buffer.getKey().equals(key)) {
                        return true;
                    } else if (buffer.next != null){
                        buffer = buffer.next;
                    } else {
                        break;
                    }
                }
            }
        }
        return false;
    }

    public boolean containsValue(Object value)  {
        for (Node<K, V> n: table) {
            if (n != null) {
                Node<K, V> buffer = n;
                while (true) {
                    if (buffer.getValue().equals(value)) {
                        return true;
                    } else if (buffer.next != null){
                        buffer = buffer.next;
                    } else {
                        break;
                    }
                }
            }
        }
        return false;
    }

    public V get(Object key) {
        for (Node<K, V> n: table) {
            if (n != null) {
                Node<K, V> buffer = n;
                while (true) {
                    if (buffer.getKey().equals(key)) {
                        return buffer.getValue();
                    } else if (buffer.next != null){
                        buffer = buffer.next;
                    } else {
                        break;
                    }
                }
            }
        }
        return null;
    }

    public V put(Object key, Object value) {
        int hash = key.hashCode();
        Node<K, V> newNode = new Node<>(hash, (K) key, (V) value, null);
        V valueToReturn;
        if (this.containsKey(key)) {
            V oldValue = this.getNode(key).value;
            this.getNode(key).value = (V) value;
            valueToReturn = oldValue;
        } else if (table != null && table[(size - 1) & hash] != null) {
            table[(size - 1) & hash].next = newNode;
            valueToReturn = (V) value;
        } else {
            table[(size - 1) & hash] = newNode;
            fill++;
            valueToReturn = (V) value;
        }
        if (fill >= maxSize) {
            table = Arrays.copyOf(table, table.length + size);
            size = size*2;
            maxSize = (int) (size*fillRatio);
        }
        return valueToReturn;
    }

    public V remove(Object key) {
        V value = null;
        for (Node<K, V> n: table) {
            if (n != null) {
                Node<K, V> buffer = n;
                while (true) {
                    if (buffer.getKey().equals(key)) {
                        value = buffer.value;
                        n = null;
                        break;
                    } else if (buffer.next != null){
                        buffer = buffer.next;
                    } else {
                        break;
                    }
                }
            }
        }
        return value;
    }

    public void putAll(Map<? extends K, ? extends V> m) {
        for (Map.Entry<? extends K, ? extends V> o: m.entrySet()) {
            this.put(o.getKey(), o.getValue());
        }
    }

    public void clear() {
        table = new Node[table.length];
        fill = 0;
    }

    public Set<K> keySet() {
        Set<K> keys = new HashSet<>();
        for (Node<K, V> n: table) {
            if (n != null) {
                Node<K, V> buffer = n;
                while (true) {
                    keys.add(buffer.getKey());
                    if (buffer.next != null){
                        buffer = buffer.next;
                    } else {
                        break;
                    }
                }
            }
        }
        return keys;
    }

    public Collection<V> values() {
        Collection<V> val = new ArrayList<>();
        for (Node<K, V> n: table) {
            if (n != null) {
                Node<K, V> buffer = n;
                while (true) {
                    val.add(buffer.getValue());
                    if (buffer.next != null){
                        buffer = buffer.next;
                    } else {
                        break;
                    }
                }
            }
        }
        return val;
    }

    public Set<Entry<K, V>> entrySet() {
        Set<Entry<K, V>> set = new HashSet<>();
        for (Node<K, V> n:table) {
            if (n != null) set.add(n);
        }
        return set;
    }

    private Node<K,V> getNode(Object key) {
        int hash = key.hashCode();

        for (Node<K, V> n: table) {
            if (n != null) {
                Node<K, V> buffer = n;
                while (true) {
                    if (buffer.getKey().equals(key)) {
                        if (buffer.hashCode() == hash) return buffer;
                    } else if (buffer.next != null){
                        buffer = buffer.next;
                    } else {
                        break;
                    }
                }
            }
        }
        return null;
    }

}
